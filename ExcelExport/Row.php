<?php

namespace Cronius\DataTableExcelBundle\ExcelExport;

use PHPExcel_Cell;

/**
 * Class Row Excel row.
 */
class Row implements \ArrayAccess, \Iterator
{
    /**
     * @var mixed
     *
     * Input object
     */
    private $inputRow;

    /**
     * @var array
     *
     * Output array to return
     */
    private $cells = array();

    /**
     * @var array array keys of cells in the required order
     */
    protected $cellOrder = array();

    /**
     * @var int Array position
     */
    private $position;

    /**
     * @var int
     *
     * Row number
     */
    private $rowNumber;

    public function rewind()
    {
        $this->position = 0;
    }

    public function current()
    {
        return $this->cells[$this->position];
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        ++$this->position;
    }

    public function valid()
    {
        return isset($this->cells[$this->position]);
    }

    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->cells[] = $value;
        } else {
            $this->cells[$offset] = $value;
        }
    }

    public function offsetExists($offset)
    {
        return isset($this->cells[$offset]);
    }

    public function offsetUnset($offset)
    {
        unset($this->cells[$offset]);
    }

    public function offsetGet($offset)
    {
        return isset($this->cells[$offset]) ? $this->cells[$offset] : null;
    }

    /**
     * @param \stdClass $row
     * @param array     $order cell order
     */
    public function __construct(\stdClass $row, $order = null)
    {
        $this->inputRow = $row;
        if ($order) {
            $this->setCellOrder($order);
        }
        $this->setCells();
    }

    /**
     * @param array $order Set cell order
     */
    public function setCellOrder(array $order)
    {
        $this->cellOrder = $order;
    }

    /**
     * @param int $number Set row number & generate cell coordinates
     */
    public function setNumber($number)
    {
        if (!is_integer($number)) {
            throw new \InvalidArgumentException(sprint('setNumber only accepts integer. Input was type %s', gettype($number)));
        }

        $this->rowNumber = $number;
        $this->generateCellCoordinates();
    }

    /**
     * @return int
     */
    public function getNumber()
    {
        return $this->rowNumber;
    }

    /**
     * Set cells.
     */
    public function setCells()
    {
        $cells = array();
        $unsortedCells = array();
        foreach ($this->inputRow as $id => $cellRow) {
            $cell = new Cell($cellRow);
            $cell->setKey($id);
            $unsortedCells[$id] = $cell;
        }

        if ($this->cellOrder) {
            foreach ($this->cellOrder as $key) {

                /*
                 * Cells with checkboxes/etc should be ignored
                 */
                if (isset($unsortedCells[$key])) {
                    $cells[] = $unsortedCells[$key];
                }
            }
        } else {
            foreach ($unsortedCells as $cell) {
                $cells[] = $cell;
            }
        }

        $this->cells = $cells;
    }

    /**
     * Generate cell coordinates.
     */
    private function generateCellCoordinates()
    {
        foreach ($this->cells as $id => $cell) {
            $letter = $this->convertToExcelColumn($id);
            $this->cells[$id]->generateCoordinates($this->rowNumber, $letter);
        }
    }

    /**
     * @param $columnNumber
     *
     * @return string column
     *
     * Convert passed number to excel column name
     */
    private function convertToExcelColumn($columnNumber)
    {
        return PHPExcel_Cell::stringFromColumnIndex($columnNumber);
    }

    /**
     * @return array
     */
    public function getRowKeys()
    {
        return array_keys($this->outputRow);
    }

    /**
     * @param array $order
     *
     * Order output row by passed keys
     */
    private function orderOutputRow(array $keys)
    {
        $orderedRow = array();
        foreach ($keys as $key) {
            $orderedRow[$key] = $this->outputRow[$key];
        }

        $this->outputRow = $orderedRow;
    }

    public function getIterator()
    {
        return new \ArrayIterator($this);
    }

    /**
     * @return array
     */
    public function getCellOrder()
    {
        return $this->cellOrder;
    }
}
