<?php

namespace Cronius\DataTableExcelBundle\ExcelExport;

use PHPExcel_Shared_Date;

/**
 * Class Cell.
 */
class Cell
{
    /**
     * @var string Datatable key of cell value
     */
    private $key;

    /**
     * @var int|string|bool|mixed|array
     *
     * Input value to parse
     */
    private $inputValue;

    /**
     * @var string|mixed
     *
     * Output value to write to return
     */
    private $outputValue;

    /**
     * @var string
     *
     * Input value type
     */
    private $inputValueType;

    /**
     * @var string
     */
    private $outputValueType;

    /**
     * @var string cell coordinates
     */
    private $cellCoordinates;

    /**
     * @var string cell coordinates column
     */
    private $cellCoordinatesColumn;

    /**
     * @param $value
     *
     * @throws \Exception
     */
    public function __construct($value)
    {
        $this->inputValue = $value;
        $this->setInputValueType();
        $this->calculateOutputValue();
    }

    /**
     * @param $row cell row
     * @param $column cell column
     *
     * Generate coordinates from passed row & column
     */
    public function GenerateCoordinates($row, $column)
    {
        if (!is_integer($row)) {
            throw new \InvalidArgumentException(sprintf('setCellCoordinates only accepts integer as row. Input was type %s', gettype($row)));
        }

        if (!is_string($column)) {
            throw new \InvalidArgumentException(sprintf('setCellCoordinates only accepts string as column. Input was type %s', gettype($column)));
        }

        $this->cellCoordinates = $column.''.$row;
        $this->cellCoordinatesColumn = $column;
    }

    /**
     * @return string
     *
     * Get Column
     */
    public function getCellCoordinatesColumn()
    {
        return $this->cellCoordinatesColumn;
    }

    /**
     * @return string
     *
     * Get coordinates
     */
    public function getCellCoordinates()
    {
        return $this->cellCoordinates;
    }

    /**
     * @return mixed
     *
     * Return cell value
     */
    public function getValue()
    {
        return $this->outputValue;
    }

    /**
     * @return string
     *
     * Return cell value type
     */
    public function getValueType()
    {
        return $this->outputValueType;
    }

    /**
     * Sets input value variable type.
     */
    private function setInputValueType()
    {
        $this->inputValueType = gettype($this->inputValue);
    }

    /**
     * @throws \Exception Doesn't know what to do with input value
     *
     * Calculate output value
     */
    private function calculateOutputValue()
    {
        switch ($this->inputValueType) {
            case 'integer':
                $this->setCellInteger();
                break;
            case 'string':
                $this->setCellString();
                break;
            case 'boolean':
                $this->setCellBool();
                break;
            case 'object':
                $this->setCellObject();
                break;
            case 'array':
                $this->setCellArray();
                break;
            default:
                //throw new \Exception(sprintf('Unknown input value type %s', $this->inputValueType));
                $this->setEmptyCell();
                break;
        }
    }

    private function setEmptyCell()
    {
        $this->outputValue = '';
        $this->outputValueType = 'empty';
    }

    /**
     * Sets output value to integer value.
     */
    private function setCellInteger()
    {
        $this->outputValue = $this->inputValue;
        $this->outputValueType = $this->inputValueType;
    }

    /**
     * Sets output value to string value of object.
     */
    private function setCellObject()
    {
        $array = get_object_vars($this->inputValue);

        if (isset($array['timestamp'])) {
            $this->outputValue = $this->getCellDate($array['timestamp']);
            $this->outputValueType = 'datetime';
        } /*
         * If the object isn't a datetime we're assuming it's a related record for now. In that case get the first attribute that isn't called id. TOFIX!
         */
        else {
            $set = false;
            foreach ($array as $key => $value) {
                if ($key != 'id') {
                    $this->outputValue = $value;
                    $this->outputValueType = 'string';
                    $set = true;
                }
            }
            if (!$set) {
                throw new \DomainException(sprintf("Don't know what to do with object"));
            }
        }
    }

    /**
     * @param $timestamp
     *
     * @return mixed
     *
     * Returns date value that phpexcel accepts
     */
    private function getCellDate($timestamp)
    {
        return PHPExcel_Shared_date::PHPToExcel($timestamp);
    }

    /**
     * Set output value to parsed string of array elements.
     */
    private function setCellArray()
    {
        if (!is_array($this->inputValue)) {
            throw new \InvalidArgumentException(sprint('setCellArray only accepts array. Input was %s', $value));
        }

        $cellValue = '';
        foreach ($this->inputValue as $linkedValue) {
            $values = get_object_vars($linkedValue);
            foreach ($values as $key => $value) {
                if ($key != 'id') {
                    $cellValue .= ' '.$values[$key];
                }
            }
        }

        $this->outputValue = $cellValue;
        $this->outputValueType = $this->inputValueType;
    }

    /**
     * Set output value to string value.
     */
    private function setCellString()
    {
        $this->outputValue = trim(strip_tags((string) $this->inputValue));
        $this->outputValueType = $this->inputValueType;
    }

    /**
     * Sets output value to boolean value.
     */
    private function setCellBool()
    {
        if (!is_bool($this->inputValue)) {
            throw new \InvalidArgumentException(sprint('setCellBool only accepts boolean. Input was of type %s', gettype($value)));
        }
        $this->outputValue = $this->inputValue;
        $this->outputValueType = $this->inputValueType;
    }

    /**
     * @param string $key
     *
     * Set cell key
     */
    public function setKey($key)
    {
        if (!is_string($key)) {
            throw new \InvalidArgumentException(sprint('setInputKey only accepts string. Input was of type %s', gettype($key)));
        }

        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }
}
