<?php

namespace Cronius\DataTableExcelBundle\ExcelExport;

use Sg\DatatablesBundle\Datatable\Data\DatatableDataManager;
use Sg\DatatablesBundle\Datatable\View\DatatableViewInterface;
use Sg\DatatablesBundle\Datatable\Data\DatatableQuery;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Serializer;

class ExcelExportDataTableManager extends DatatableDataManager
{
    private $params;

    private $serializer;

    private $configs;

    public function __construct(RequestStack $requestStack, Serializer $serializer, array $configs)
    {
        parent::__construct($requestStack, $serializer, $configs);
        $this->serializer = $serializer;
        $this->configs = $configs;
    }

    /**
     * Set params.
     *
     * @param array $params
     */
    public function setParams(array $params)
    {
        $this->params = $params;
    }

    /**
     * Query Datafrome from passed params.
     *
     * @param DatatableViewInterface $datatableView
     *
     * @return DatatableQuery
     */
    public function getQueryFrom(DatatableViewInterface $datatableView)
    {
        if (!$this->params) {
            throw new \Exception(sprintf("params aren't set yet"));
        }

        $query = new DatatableQuery($this->serializer, $this->params, $datatableView, $this->configs);

        return $query;
    }
}
