<?php

namespace Cronius\DataTableExcelBundle\ExcelExport;

use Doctrine\ORM\QueryBuilder;

interface OverrideQueryInterface
{
    /**
     * @param QueryBuilder $old_query
     *
     * @return mixed
     *
     * Override the datatable query
     */
    public function overrideQuery(QueryBuilder $old_query);
}
