<?php

namespace Cronius\DataTableExcelBundle\ExcelExport;

/**
 * Class ColumnRow Column row.
 */
class ColumnRow extends Row
{
    /**
     * @var mixed
     *
     * array of datatable columns
     */
    private $inputColumns;

    /**
     * @var array
     *
     * array of column names
     */
    private $outputColumns;

    /**
     * @param mixed $columns datatable columns
     */
    public function __construct(array $columns)
    {
        $this->inputColumns = $columns;
        $this->setOutputColumns();
        $stdRow = $this->createStdRowFromArray();
        $this->generateCellOrder();

        parent::__construct($stdRow);
    }

    /**
     * Set column labels.
     */
    private function setOutputColumns()
    {
        foreach ($this->inputColumns as $column) {
            if ($column->getData()) {
                $name = explode('.', $column->getDql());
                $this->outputColumns[$name[0]] = strip_tags($column->getTitle());
            }
        }
    }

    /**
     * @return object
     *
     * Convert columns to std class
     */
    private function createStdRowFromArray()
    {
        return (object) $this->outputColumns;
    }

    /**
     * Generates cell order from output columns.
     */
    private function generateCellOrder()
    {
        $this->cellOrder = array_keys($this->outputColumns);
    }
}
