<?php

namespace Cronius\DataTableExcelBundle\ExcelExport;

use AppBundle\Service\MyDataTableManager;
use Doctrine\ORM\Query;
use Sg\DatatablesBundle\Datatable\Data\DatatableQuery;
use Sg\DatatablesBundle\Datatable\View\DatatableViewInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

class ExcelExport
{
    /**
     * @var mixed data extracted from json
     */
    private $data;

    /**
     * @var mixed array of json reply
     */
    private $json;

    /**
     * @var \Liuggio\ExcelBundle\Factory phpExcel
     */
    private $phpExcel;

    /**
     * @var \PHPExcel the spreadsheet object
     */
    private $phpExcelObject;

    /**
     * @var \Symfony\Component\HttpFoundation\RequestStack
     */
    private $requestStack;

    /**
     * @var Datatable object
     */
    private $dataTable;

    /**
     * @var MyDataTableManager
     */
    private $dataTableQuery;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var mixed params passed to MyDataTableManager
     */
    private $params;

    /**
     * @var Rows
     */
    private $rows;

    /**
     * @var PHPExcel_Writer_Excel2007
     */
    private $writer;

    /**
     * @var string directory excel is saved to
     */
    private $saveDir;

    /**
     * @var string excel filename
     */
    private $fileName;

    /**
     * @var Query
     */
    private $query;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->phpExcel = $container->get('phpexcel');
        $this->requestStack = $container->get('request_stack');
        $this->rows = new Rows();
        $this->dataTableQuery = $container->get('cronius.data_table_excel.data_table_manager');
        $this->saveDir = 'uploads/excelExport';

        $this->generateFileName();
    }

    /**
     * @param DatatableQuery $query
     *
     * Set the query
     */
    public function setQuery(DatatableQuery $query)
    {
        $this->query = $query;
    }

    private function generateFileName()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';
        for ($i = 0; $i < 20; ++$i) {
            $randstring .= $characters[rand(0, strlen($characters) - 1)];
        }

        $this->fileName = $randstring.'.xls';
    }

    /**
     * Set datatable and response data.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    private function getResponse()
    {
        $response = $this->query->getResponse();
        $this->setJson($response);

        return $response;
    }

    /**
     * @throws \Exception
     *
     * Generate controller from dataTable class if overrideQuery exists
     */
    private function generateQuery()
    {
        if (!$this->query) {
            $this->query = $this->dataTableQuery->getQueryFrom($this->dataTable);
        }

        if (method_exists($this->dataTable, 'overrideQuery')) {
            $newQuery = $this->dataTable->overrideQuery($this->query->getQuery());
            $this->query->setQuery($newQuery);
        }
    }

    /**
     * Set Excel data.
     *
     * @param string $table_name
     */
    public function setData(DatatableViewInterface $table)
    {
        $this->dataTable = $table;
        $this->generateQuery();

        $this->getResponse();

        $this->setHeaders();
        $this->setBody();

        $this->setupExcel();
        $this->writeExcel();
        $this->autosizeExcel();
    }

    /**
     * Resize the cells automatically.
     */
    private function autosizeExcel()
    {
        if (isset($this->rows[0])) {
            foreach ($this->rows[0] as $cell) {
                $this->phpExcelObject->getActiveSheet()
                    ->getColumnDimension($cell->getCellCoordinatesColumn())
                    ->setAutoSize(true);
            }
        }
    }

    /**
     * Set Excel body.
     */
    private function setBody()
    {
        foreach ($this->data as $rowNumber => $row) {
            $this->rows[] = new Row($row);
        }
    }

    /**
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function getData()
    {
        return $this->phpExcel->createStreamedResponse($this->writer);
    }

    /**
     * @param array $params
     *
     * pass params
     */
    public function setParams(array $params)
    {
        $this->stripAndSetParams($params);
    }

    /**
     * @param array $params
     *
     * Strip unnecessary param elements and save
     */
    private function stripAndSetParams(array $params)
    {
        $params['length'] = null;
        $params['start'] = null;

        $this->params = $params;
        $this->dataTableQuery->setParams($params);
    }

    /**
     * Set the headers row.
     */
    private function setHeaders()
    {
        $row = new ColumnRow($this->dataTable->getColumnBuilder()->getColumns());
        $this->rows->setCellOrder($row->getCellOrder());
        $this->rows[] = $row;
    }

    /**
     * @throws \Exception
     *
     * Write rows to excel
     */
    private function writeExcel()
    {
        foreach ($this->rows as $row) {
            foreach ($row as $cell) {
                $this->writeExcelCell($cell);
            }
        }

        $this->writer = $this->phpExcel->createWriter($this->phpExcelObject, 'Excel2007');
    }

    /**
     * @param Cell $cell
     *
     * @throws \Exception
     *
     * Write Excel cell
     */
    private function writeExcelCell(Cell $cell)
    {
        switch ($cell->getValueType()) {
            case 'string':
                break;
            case 'integer':
                break;
            case 'boolean':
                break;
            case 'double':
                break;
            case 'array':
                break;
            case 'datetime':
                $this->phpExcelObject->getActiveSheet()
                    ->getStyle($cell->getCellCoordinates())
                    ->getNumberFormat()
                    ->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2);
                break;
            case 'empty':
                break;
            default:
                throw new \Exception(sprintf("don't know what to do with value type %s", $cell->getValueType()));
                break;
        }

        $value = $cell->getValue();

        if ($value instanceof \stdClass) {
            $class = $value;
            foreach ($class as $key => $value) {
                $value = $class->$key;
                break;
            }
        }

        $this->phpExcelObject->getActiveSheet()
            ->setCellValue($cell->getCellCoordinates(), $value);
    }

    /**
     * Create new PHPEXcel object.
     *
     * @throws \PHPExcel_Exception
     */
    private function setupExcel()
    {
        $this->phpExcelObject = $this->phpExcel->createPHPExcelObject();
        $this->phpExcelObject->setActiveSheetIndex(0);
    }

    /**
     * Get parsed json from Response.
     *
     * @param Response $response
     */
    private function setJson(Response $response)
    {
        $this->json = json_decode($response->getContent());
        $data = get_object_vars($this->json);
        $this->data = $data['data'];
    }

    /**
     * @param string $query querystring
     *
     * Set params from querystring
     */
    public function setQueryString($query)
    {
        parse_str($query, $vars);
        $this->stripAndSetParams($vars);
    }

    /**
     * @return string file path
     *
     * Save excel to filesystem
     */
    public function saveFile()
    {
        $fs = new Filesystem();

        $strippedDir = rtrim($this->saveDir, '/');
        $strippedFilename = trim($this->fileName, '/');
        $path = $this->container->getParameter('kernel.root_dir').$strippedDir;
        $file = $path.'/'.$strippedFilename;

        if (!$fs->exists($path)) {
            try {
                $fs->mkdir($path, 0770);
            } catch (IOExceptionInterface $e) {
                echo 'An error occurred while creating your directory at '.$e->getPath();
            }
        }

        $this->writer->save($file);

        return $file;
    }

    /**
     * @param $save_dir string excel save dir
     *
     * Set save directory
     */
    public function setSaveDir($save_dir)
    {
        $this->saveDir = $save_dir;
    }

    /**
     * @param $file_name string excel file name
     */
    public function setFileName($file_name)
    {
        $this->fileName = $file_name;
    }
}
