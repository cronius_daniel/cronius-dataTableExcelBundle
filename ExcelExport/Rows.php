<?php

namespace Cronius\DataTableExcelBundle\ExcelExport;

/**
 * Class Rows rows used in excel.
 */
class Rows implements \ArrayAccess, \Iterator
{
    /**
     * @var array Rows
     */
    private $container = array();

    /**
     * @var int Array position
     */
    private $position;

    /**
     * @var array order of cells in rows
     */
    private $cellOrder;

    public function __construct()
    {
        $this->position = 0;
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function current()
    {
        return $this->container[$this->position];
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        ++$this->position;
    }

    public function valid()
    {
        return isset($this->container[$this->position]);
    }

    /**
     * @return int Generate number for new row
     */
    private function generateRowNumber()
    {
        return count($this->container) + 1;
    }

    /**
     * @param array $order Set cell order
     */
    public function setCellOrder(array $order)
    {
        $this->cellOrder = $order;
    }

    public function offsetSet($offset, $value)
    {
        $rowNumber = $this->generateRowNumber();
        $value->setCellOrder($this->cellOrder);
        $value->setCells($this->cellOrder);
        $value->setNumber($rowNumber);

        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }
}
