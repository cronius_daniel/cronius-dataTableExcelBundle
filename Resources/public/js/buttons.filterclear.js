/**
 * http://www.datatables.net/plug-ins/api/fnFilterClear
 */
$.fn.dataTable.ext.buttons.filterClear = {
    text: 'Filters wissen',

    action: function ( e, dt, node, config ) {
        dt
            .search( '' )
            .columns().search( '' )
            .draw();
    }
};
