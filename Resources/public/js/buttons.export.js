$.fn.dataTable.ext.buttons.export = {
    text: 'Export naar Excel',

    action: function ( e, dt, node, config ) {
        var url = dt.ajax.url();
        var params = dt.ajax.params();

        params['export'] = 'excel';

        $.ajax(url, {
            method: config.method,
            data: params,
            success: function(data) {
                window.location = data.url
            }
        });
    }
};
